# -*- coding: utf-8 -*-
"""
Created on Thu Nov 12 15:55:58 2015

@author: Filip
"""
import re

names = []

fopen = open('mbox-short.txt')

for line in fopen:
    line = line.rstrip()
    lst = re.findall('([^ <]\S+)@\S+', line)
    if len(lst) > 0:
        names.append(lst)

for name in names:
    print name
        
