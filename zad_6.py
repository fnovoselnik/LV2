# -*- coding: utf-8 -*-
"""
Created on Fri Nov 20 09:54:11 2015

@author: Filip
"""

from bs4 import BeautifulSoup
import urllib2
import matplotlib.pyplot as plt


url = 'https://en.wikipedia.org/wiki/Demographics_of_Croatia'
html = urllib2.urlopen(url).read()
soup = BeautifulSoup(html)

year=[] #lista u koju spremamo godine
population=[] #lista u koju spremamo broj stanovnika


table = soup.find("table", { "class" : "wikitable" }) #tag table with "wikitable" class
    
for row in table.findAll("tr"): #za svaki red trazimo tagove
    cells = row.findAll("td")
    if len(cells) == 5:
        ye = cells[0].find(text=True) #nalazimo stupac s godinama
        den = cells[1].find(text=True) #nalazimo stupac s brojem stanovnika
        den_parse = den.replace(",","") #parsiramo godinu da bi mogli plotat jer su znamenke odvojene zarezom
        year.append(ye)
        population.append(den_parse)


#plotamo
plt.plot(year,density, 'b')
plt.xlabel('Year')
plt.ylabel('Enumerated population')

        