# -*- coding: utf-8 -*-
"""
Created on Wed Nov 18 17:49:40 2015

@author: Filip
"""

import csv
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm

mpg = []
hp = []
wt = []

file = open('mtcars.csv')

jet=plt.get_cmap('coolwarm')

firstLine = True

for line in file:
    if firstLine:  #preskacemo prvu liniju
        firstLine = False
        continue
    linesplit = line.split(',') #razdvajamo podatke po zarezu
    mpg.append(linesplit[1])
    hp.append(linesplit[4])
    wt.append(linesplit[6])


plt.scatter(mpg,hp) #plotanje
plt.xlabel('mpg')
plt.ylabel('hp')

#U ovom dijelu sam pokusao dodati i tezinu vozila kao funkciju boje na grafu (color coded)
#
#cm = mpl.cm.get_cmap('RdYlBu') 
#sc = plt.scatter(mpg, hp, c=wt, vmin=min(wt), vmax=max(wt), cmap=cm) 
#plt.colorbar(sc) 


